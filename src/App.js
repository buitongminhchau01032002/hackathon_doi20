import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Info from './components/Info';
import Row from './components/Row';
import Rate from './components/Rate';
import OurInfomation from './components/OurInfomation';
import HeaderContact from './components/HeaderContact';
import HeaderCourse from './components/HeaderCourse';
import Courses from './components/Courses';
import NMLT from './components/courses-detail/NMLTcourse';
import DiffentCourse from './components/courses-detail/DiffentCourse';
import Management from './components/teacher/manage-student';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
function App() {
    return (
        <div className>
            <Router>
                <Header />
                <Switch>
                    <Route path='/' exact>
                        <Row />
                        <Info />
                        <Rate />
                    </Route>
                    <Route path='/courses' exact>
                        <HeaderCourse />
                        <Courses />
                    </Route>
                    <Route path='/contact' exact>
                        <HeaderContact />
                        <OurInfomation />
                    </Route>
                    <Route path='/course-detail/nmlt' exact>
                        <HeaderCourse />
                        <NMLT />
                    </Route>
                    <Route path='/course-detail/:id'>
                        <DiffentCourse />
                    </Route>
                    <Route path='/course-management/nmlt' exact>
                        <HeaderCourse />
                        <Management />
                    </Route>
                </Switch>
            </Router>
            <Footer />
        </div>
    );
}

export default App;
