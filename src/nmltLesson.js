const lessons = [
    {
        slug: 'bai1',
        name: 'Bài 1: Giới thiệu',
        description:
            'C++ là một ngôn ngữ lập trinh bậc trung. Nó có nghĩa là bạn có thể sử dụng C++ để phát triển những ứng dụng bậc cao, và cả những chương trình bậc thấp hoạt động tốt trên phần cứng. C++ là một ngôn ngữ lập trình hướng đối tượng. Khác với ngôn ngữ lập trình C - một ngôn ngữ lập trình hướng thủ tục, chương trình được tổ chức theo thuật ngữ "chức năng", một chức năng gồm có những hành động mà bạn muốn làm. C++ được thiết kế với một cách tiếp cận hoàn toàn mới được gọi là lập trình hướng đối tượng, nơi mà chúng ta sử dụng những đối tượng, các lớp và sử dụng các khái niệm như: thừa kế, đa hình, tính đóng gói, tính trừu tượng ... Những khái niệm này khá phức tạp, nên nếu bạn chưa hiểu về chúng, đừng lo lắng, chúng ta sẽ lần lượt làm rõ từng khái niệm trong mỗi bài học khác nhau. C++ là một ngôn ngữ lập trình hướng cấu trúc giống ngôn ngữ C, nó có nghĩa là chúng ta có thể tổ chức chương trình trên khái niệm functions. C++ có thể chạy trên nhiều nền tảng khác nhau như Windows, Mac OS, một số biến thể của UNIX...',
        students: [
            {
                name: 'Minh Châu',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn A',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn A',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn A',
                submitedStatus: false,
            },
        ],
    },
    {
        slug: 'bai2',
        name: 'Bài 2: Chương trình đầu tiên',
        description:
            'Chúng ta đã biết về quy trình làm việc để tạo ra một chương trình C++, những công cụ cần thiết và IDE mà chúng ta sẽ sử dụng để phát triển chương trình. Đến đây chắc các bạn cũng đang háo hức muốn bắt tay vào viết một cái gì đó. Trong bài này, chúng ta sẽ cùng viết một chương trình mà bất cứ lập trình viên C++ nào cũng từng trải qua. Một chương trình huyền thoại mang tên "Hello World".',
        students: [
            {
                name: 'Đạt Đỗ',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn B',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn B',
                submitedStatus: false,
            },
            {
                name: 'Nguyễn B',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn B',
                submitedStatus: false,
            },
        ],
    },
    {
        slug: 'bai3',
        name: 'Bài 3: Biến',
        description:
            'Chào tất cả các bạn đang theo dõi khóa học lập trình trực tuyến ngôn ngữ C++. Tiếp tục với bài học ngày hôm nay, chúng ta sẽ cùng tìm hiểu về một cách tổ chức dữ liệu cơ bản trong thiết bị lưu trữ tạm thời của máy tính giúp khắc phục một số nhược điểm của việc sử dụng các biến thông thường. Đặt vấn đề Giảng viên cần tìm ra điểm số cao nhất của bài kiểm tra môn lập trình cơ sở. Giả sử lớp học có 30 sinh viên có số thứ tự 1 đến 30.Công việc của những lập trình viên chúng ta là giúp giảng viên này chỉ ra số thứ tự của sinh viên có điểm kiểm tra cao nhất, và điểm cao nhất đó là bao nhiêu bằng cách viết chương trình ngôn ngữ C++ trên máy tính để tiết kiệm thời gian suy nghĩ.\nTìm hướng giải quyết\nVới yêu cầu như trên, chúng ta cần 30 biến để lưu lại điểm của 30 sinh viên.',
        students: [
            {
                name: 'Đức Thịnh',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn A',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn A',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn A',
                submitedStatus: false,
            },
        ],
    },
    {
        slug: 'bai4',
        name: 'Bài 4: Mảng',
        description: ' Nội dung của Bài 4',

        students: [
            {
                name: 'Nguyễn Văn C',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn C',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn C',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn C',
                submitedStatus: false,
            },
        ],
    },
    {
        slug: 'bai5',
        name: 'Bài 5: Con Trỏ',
        description: ' Nội dung của Bài 5',

        students: [
            {
                name: 'Nguyễn Văn D',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn D',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn D',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn D',
                submitedStatus: false,
            },
        ],
    },
    {
        slug: 'bai6',
        name: 'Bài 6: Struct',
        description: ' Nội dung của Bài 6',

        students: [
            {
                name: 'Nguyễn Văn E',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn E',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn E',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn E',
                submitedStatus: false,
            },
        ],
    },
    {
        slug: 'bai7',
        name: 'Bài 7: Class',
        description: ' Nội dung của Bài 7',

        students: [
            {
                name: 'Nguyễn Văn F',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn F',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn F',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn F',
                submitedStatus: false,
            },
        ],
    },
    {
        slug: 'bai8',
        name: 'Bài 8: Linked List',
        description: ' Nội dung của Bài 8',

        students: [
            {
                name: 'Nguyễn Văn G',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn G',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn G',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn G',
                submitedStatus: false,
            },
        ],
    },
    {
        slug: 'bai9',
        name: 'Bài 9: OOP',
        description: ' Nội dung của Bài 9',

        students: [
            {
                name: 'Nguyễn Văn H',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn H',
                submitedStatus: false,
            },
            {
                name: 'Nguyễn Văn H',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn H',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn H',
                submitedStatus: false,
            },
        ],
    },
    {
        slug: 'bai10',
        name: 'Bài 10: Giới thiệu',
        description: ' Nội dung của Bài 10',
        students: [
            {
                name: 'Nguyễn Văn K',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn K',
                submitedStatus: false,
            },
            {
                name: 'Nguyễn Văn K',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn K',
                submitedStatus: false,
            },
        ],
    },
    {
        slug: 'bai11',
        name: 'Bài 11: Binary Search',
        description: ' Nội dung của Bài 11',
        students: [
            {
                name: 'Nguyễn Văn L',
                submitedStatus: false,
            },
            {
                name: 'Nguyễn Văn L',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn L',
                submitedStatus: true,
            },
            {
                name: 'Nguyễn Văn L',
                submitedStatus: false,
            },
            {
                name: 'Nguyễn Tiến',
                submitedStatus: false,
            },
        ],
    },
];
export default lessons;
