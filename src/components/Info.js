import React from 'react'
import "../App.css"
import teacher from "../img/teacher.jpg"
import student from "../img/student-group.jpg"
function Info() {
    return (
        <div>
            <section className="section-teacher u-padding-body">
                <div className="section-teacher__left">
                    <h2 className="section-teacher__heading heading-primary">
                        Giảng viên xây dựng khoá học, bài giảng trực tuyến
                    </h2>
                            <p className="section-teacher__text para">
                                Hỗ trợ đầy đủ những công cụ giúp giảng viên dễ dàng tạo ra những khóa học, bài giảng trực tuyến nhầm đáp ứng những nhu cầu về kiến thức cho học sinh và sinh viên.
                    </p>
                            <div className="section-teacher__btn">
                                <a href="#" className="btn-outline section-teacher__signup">
                                    Đăng ký
                                </a>
                                <a href className="btn section-teacher__login">
                                    Đăng nhập
                                </a>
                            </div>
                        </div>
                        <div className="section-teacher__right">
                            <img src={teacher} alt="" className="section-teacher__img" />
                        </div>
                    </section>
                    <section className="section-learner u-padding-body">
                        <div className="section-learner__left">
                            <img src={student} alt="" className="section-learner__img" />
                        </div>
                        <div className="section-learner__right">
                            <h2 className="section-learner__heading heading-primary">
                                Học sinh tiếp cận với kiến thức mới ở bất kì nơi đâu.
                    </h2>
                            <p className="section-learner__text para">
                                Học tập trực tuyến với những giáo viên được trang bị đầy đủ những công cụ hỗ trợ dạy và giảng học. Tạo một môi trường học tập và quản lí thời gian hiệu quả.
                    </p>
                    <div className="section-learner__btn">
                        <a href="#" className="btn-outline section-learner__signup">
                            Đăng ký
                        </a>
                        <a href className="btn section-learner__login">
                            Đăng nhập
                        </a>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default Info
