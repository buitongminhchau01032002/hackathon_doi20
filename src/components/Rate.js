import React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import avatar from '../img/avatar.png';
function Rate(props) {
    return (
        <div>
            <section className='section-rate'>
                <div className='section-rate__content'>
                    <h2 className='section-rate__heading heading-primary'>
                        Mọi người đánh giá
                    </h2>
                    <Carousel interval = {4000} className='section-rate__crs'>
                        <Carousel.Item>
                            <div className='section-rate__block'>
                                <div className='section-rate__avatar'>
                                    <img
                                        src={avatar}
                                        alt=''
                                        className='section-rate__avatar-img'
                                    />
                                </div>
                                <p className='section-rate__name'>
                                    Minh Châu
                                </p>
                                <div className='section-rate__tag'>
                                    Giảng viên
                                </div>
                                <p className='section-rate__comment para'>
                                    Nền tảng này rất hữu ích. Tôi có thể tạo các khoá học cho học viên và tương tác với các học viên dễ dàng hơn.
                                </p>
                            </div>
                        </Carousel.Item>
                        <Carousel.Item> 
                            <div className='section-rate__block'>
                                <div className='section-rate__avatar'>
                                    <img
                                        src={avatar}
                                        alt=''
                                        className='section-rate__avatar-img'
                                    />
                                </div>
                                <p className='section-rate__name'>
                                    Đạt Đỗ
                                </p>
                                <div className='section-rate__tag'>
                                    Giảng viên
                                </div>
                                <p className='section-rate__comment para'>
                                    Tôi chỉ muốn nói rằng đây là một nền tảng tuyệt vời. Cảm ơn đội ngũ phát triển nền tảng.

                                </p>
                            </div>
                        </Carousel.Item>
                        <Carousel.Item>
                            <div className='section-rate__block'>
                                <div className='section-rate__avatar'>
                                    <img
                                        src={avatar}
                                        alt=''
                                        className='section-rate__avatar-img'
                                    />
                                </div>
                                <p className='section-rate__name'>
                                   Đức Thịnh
                                </p>
                                <div className='section-rate__tag'>
                                    Học viên
                                </div>
                                <p className='section-rate__comment para'>
                                    Thật tuyệt vời khi biết đến nền tảng này. Tôi đang học tập cùng các giảng viên. Nhiều bạn bè của tôi cũng rất hài lòng khi sử dụng nền tảng
                                </p>
                            </div>
                        </Carousel.Item>
                    </Carousel>
                </div>
            </section>
        </div>
    );
}

Rate.propTypes = {};

export default Rate;
