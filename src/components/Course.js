import React from 'react';
import PropTypes from 'prop-types';
import cover from '../img/cover.jpg';
import chinhtri from '../img/ChinhTri.jpg';
import it from '../img/IT.jpg';
import math from '../img/ToanHoc.jpg';
function Course({ course }) {
    const slug = `/course-detail/${course.slug}`;
    let image;
    switch (course.image) {
        case 1:
            image = it;
            break;

        case 2:
            image = math;
            break;

        case 3:
            image = chinhtri;
            break;

        default:
            break;
    }
    return (
        <div className='col-lg-4 col-md-6 col-sm-12'>
            <div className='card-course'>
                <div className='card-course__cover'>
                    <img
                        src={image}
                        alt=''
                        className='card-course__cover-img'
                    />
                </div>
                <div className='card-course__content'>
                    <a
                        href={slug}
                        className='card-course__title heading-primary'>
                        {course.name}
                    </a>
                    <div className='card-course__learner-heart'>
                        <div className='card-course__learner para'>
                            <i className='fas fa-user-friends'></i>{' '}
                            {course.quantities}
                        </div>
                        <div className='card-course__heart para'>
                            <i className='fas fa-heart'></i> {course.like}
                        </div>
                    </div>
                    <div className='card-course__id para'>ID: {course.id}</div>
                    <div className='card-course__teacher para'>
                        <i className='fas fa-chalkboard-teacher'></i> {course.teacherName}
                    </div>
                </div>
            </div>
        </div>
    );
}

Course.propTypes = {};

export default Course;
