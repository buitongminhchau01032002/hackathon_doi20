import React from 'react';

import avatar from '../../img/avatar.png';

function ShowStudent({ students }) {
    return (
        <div class='manage-learner'>
            {students.map((student) => {
                if (student.submitedStatus) {
                    return (
                        <div class='manage-learner-item'>
                            <div class='manage-learner-item__acc'>
                                <div class='manage-learner-item__avatar'>
                                    <img
                                        src={avatar}
                                        alt=''
                                        class='manage-learner-item__avatar-img'
                                    />
                                </div>
                                <div class='manage-learner-item__name'>
                                    {student.name}
                                </div>
                            </div>
                            <div class='manage-learner-item__status'>
                                <div class='manage-learner-item__submitted'>
                                    <div class='manage-learner-item__submitted--sm'>
                                        Đã nộp bài
                                    </div>
                                    <div class='manage-learner-item__submitted--date'>
                                        1/1/2021
                                    </div>
                                </div>
                                <div class='manage-learner-item__btn btn'>
                                    Xem
                                </div>
                            </div>
                        </div>
                    );
                } else {
                    return (
                        <div class='manage-learner-item'>
                            <div class='manage-learner-item__acc'>
                                <div class='manage-learner-item__avatar'>
                                    <img
                                        src={avatar}
                                        alt=''
                                        class='manage-learner-item__avatar-img'
                                    />
                                </div>
                                <div class='manage-learner-item__name'>
                                    {student.name}
                                </div>
                            </div>
                            <div class='manage-learner-item__status'>
                                <div class='manage-learner-item__not-submited'>
                                    Chưa nộp bài
                                </div>
                            </div>
                        </div>
                    );
                }
            })}
        </div>
    );
}

ShowStudent.propTypes = {};

export default ShowStudent;
