import React, { useState } from 'react';

import lessons from '../../nmltLesson';
import ShowStudent from './ShowStudent';
function Management(props) {
    const [students, setStudents] = useState([]);
    const handleShowLesson = (slug) => {
        let newCourse;
        lessons.map((lesson) => {
            if (lesson.slug === slug) {
                newCourse = { ...lesson };
            }
        });
        setStudents(newCourse.students);
    };
    return (
        <div>
            <div className='course-detail-info'>
                <div className='course-detail-info__title'>
                    Nhập môn lập trình
                </div>
                <div className='course-detail-info__id'>ID: 11A3B3</div>
                <div className='course-detail-info__learner-heart'>
                    <div className='course-detail-info__learner para'>
                        <i className='fas fa-user-friends'></i> 105
                    </div>
                    <div className='course-detail-info__heart para'>
                        <i className='fas fa-heart'></i> 90
                    </div>
                </div>
                <div className='course-detail-info__teacher para'>
                    <i className='fas fa-chalkboard-teacher'></i> Nguyễn Văn C
                </div>
            </div>

            <div className='course-detail-main u-padding-body'>
                <input type='checkbox' id='toggle-list-lesson' />
                <label for='toggle-list-lesson' class='toggle-list-lesson'>
                    <span class='fa fa-bars'></span>
                </label>

                <div className='list-lesson'>
                    <div className='list-lesson__heading heading-primary'>
                        Danh sách bài
                    </div>
                    <div className='list-lesson__list'>
                        {lessons.map((lesson, index) => {
                            return (
                                <p
                                    className='lesson-item'
                                    onClick={() =>
                                        handleShowLesson(lesson.slug)
                                    }
                                    key={index}>
                                    {lesson.name}
                                </p>
                            );
                        })}
                    </div>
                </div>
                <div className='lesson-detail'>
                    <ShowStudent students={students} />
                </div>
            </div>
        </div>
    );
}

Management.propTypes = {};

export default Management;
