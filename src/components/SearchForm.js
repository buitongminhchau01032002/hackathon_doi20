import React from 'react';
import PropTypes from 'prop-types';

function SearchForm({ searchTerm, handleChange }) {
    return (
        <div className='search'>
            <div className='search-form'>
                <input
                    type='search'
                    name=''
                    className='search-input'
                    placeholder='Tìm kiếm khoá học'
                    value={searchTerm}
                    onChange = {handleChange}
                />
                <button className='search-btn'>
                    <i className='fas fa-search'></i>
                </button>
            </div>
        </div>
    );
}

SearchForm.propTypes = {};

export default SearchForm;
