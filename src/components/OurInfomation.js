import React from 'react'

function OurInfomation() {
    return (
        <div>
            <div className="contact u-padding-body">
        <div className="contact-info">
          <div className="contact-heading heading-primary">
            Thông tin liên hệ
          </div>
          <p className="contact-heading-sub para">Liên hệ với chúng tôi</p>
          <div className="contact-info__group">
            <p className="contact-info__group--title para">Điện thoại:</p>
            <p className="contact-info__group--text para">0123456789</p>
          </div>
          <div className="contact-info__group">
            <p className="contact-info__group--title para">Facebook:</p>
            <p className="contact-info__group--text para">facebook.com/courses</p>
          </div>
          <div className="contact-info__group">
            <p className="contact-info__group--title para">Email:</p>
            <p className="contact-info__group--text para">courses@gmail.com</p>
          </div>
        </div>
        <div className="contact-feedback">
          <div className="contact-heading heading-primary">
            Gửi phản hồi
          </div>
          <p className="contact-heading-sub para">Để lại lời nhắn cho chúng tôi</p>
          <form className="form-contact">
            <input type="email" name placeholder="Email*" className="form-contact__textbox" />
            <input type="text" name placeholder="Họ và tên*" className="form-contact__textbox" />
            <div className="form-contact__radio-group">
              <div className="radio-group">
                <input type="radio" name="role" className="form-contact__radio" id="radio-teacher" />
                <label htmlFor="radio-teacher" className="radio-lable">
                  <span className="radio-lable__icon"><span className="radio-lable__icon--add" /></span>
                  <span className="para">Giảng viên</span>
                </label>
              </div>
              <div className="radio-group">
                <input type="radio" name="role" className="form-contact__radio" id="radio-learner" />
                <label htmlFor="radio-learner" className="radio-lable">
                  <span className="radio-lable__icon"><span className="radio-lable__icon--add" /></span>
                  <span className="para">Học viên</span>
                </label>
              </div>
            </div>
            <textarea cols={30} rows={5} placeholder="Phản hồi*" className="form-contact__textarea" defaultValue={""} />
            <div>
              <a href className="btn form-contact__submit">Gửi</a>
            </div>
          </form>
        </div>
      </div>
        </div>
    )
}

export default OurInfomation
