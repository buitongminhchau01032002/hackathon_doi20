import React from 'react';
import logo from '../img/logo.png';

function Header() {
    return (
        <div>
            <header className='header u-padding-body'>
                <div className='header__logo'>
                    <a href='/' className='logo-link'>
                        <img src={logo} alt='' className='logo-img' />
                    </a>
                </div>
                <input type='checkbox' id='toggle-menu' />
                <label for='toggle-menu' className='toggle-menu'>
                    <span className='fa fa-bars toggle-menu--open'></span>
                    <span className='fa fa-times toggle-menu--close'></span>
                </label>
                <div className='nav'>
                    <a href='/' className='menu-item menu-active'>
                        TRANG CHỦ
                    </a>
                    <a href='/courses' className='menu-item'>
                        KHOÁ HỌC
                    </a>
                    <a href='/contact' className='menu-item'>
                        LIÊN HỆ
                    </a>
                    <a href='/course-management/nmlt' className='menu-item'>
                        QUẢN LÍ KHÓA HỌC
                    </a>
                    <a href='/signup' className='btn-outline btn-nav-signup'>
                        ĐĂNG KÝ
                    </a>
                    <a href='/signin' className='btn btn-nav-login'>
                        ĐĂNG NHẬP
                    </a>
                </div>
            </header>
        </div>
    );
}

export default Header;
