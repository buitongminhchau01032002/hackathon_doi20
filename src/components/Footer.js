import React from 'react';
import PropTypes from 'prop-types';

function Footer(props) {
    return (
        <div>
            <footer class="footer u-padding-body">
                <div class="footer__col1">
                    <div class="footer__heading heading-primary">
                        Giới thiệu
                    </div>
                    <p class="footer__text footer__intro para">
                        Nền tảng của chúng tôi giúp giảng viên và các học viên được tiếp cận với một phương pháp học tập và dạy học cực kì mới mẻ. Thấu hiểu được tâm lý chung của những học viên và giảng viên, chúng tôi luôn hỗ trợ, luôn lắng nghe và đảm bảo một môi trường học tập và rèn luyện toàn diện. 
                    </p>
                </div>
                <div class="footer__col2">
                    <div class="footer__heading heading-primary">
                        Liên hệ
                    </div>
                    <p class="footer__text para">
                        <i class="fas fa-phone-alt"></i> 0123456789
                    </p>
                    <a href="" class="footer__link para">
                        <i class="fab fa-facebook-square"></i> facebook.com/courses
                    </a>
                    <a href="" class="footer__link para">
                        <i class="fas fa-envelope"></i> courses@gmail.com
                    </a>
                </div>
                <div class="footer__col3">
                    <div class="footer__heading heading-primary">
                        Truy cập nhanh
                    </div>
                    <a href="/" class="footer__link para">
                        Trang chủ
                    </a>
                    <a href="/courses" class="footer__link para">
                        Khoá học
                    </a>
                    <a href="/contact" class="footer__link para">
                        Liên hệ
                    </a>
                </div>
            </footer>
        </div>
    );
}

Footer.propTypes = {};

export default Footer;
