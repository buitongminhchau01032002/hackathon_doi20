import React from 'react';

function ShowLesson({ course }) {
    return (
        <div>
            <h1 className='lesson-title'>{course.name}</h1>
            <h3> {course.description}</h3>
        </div>
    );
}

ShowLesson.propTypes = {};

export default ShowLesson;
