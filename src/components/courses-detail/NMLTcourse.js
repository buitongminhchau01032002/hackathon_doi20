import React, { useState } from 'react';
import lessons from '../../nmltLesson';
import ShowLesson from './showLesson';
import ShowComment from './ShowComment';
import avatar from '../../img/avatar.png';
function NMLT(props) {
    const [course, setCourse] = useState({});
    const handleShowLesson = (slug) => {
        let newCourse;
        lessons.map((lesson) => {
            if (lesson.slug === slug) {
                newCourse = { ...lesson };
                console.log(lesson);
            }
        });
        setCourse(newCourse);
    };
    return (
        <div>
            <div className='course-detail-info'>
                <div className='course-detail-info__title'>
                    Nhập môn lập trình
                </div>
                <div className='course-detail-info__id'>ID: 1000</div>
                <div className='course-detail-info__learner-heart'>
                    <div className='course-detail-info__learner para'>
                        <i className='fas fa-user-friends'></i> 105
                    </div>
                    <div className='course-detail-info__heart para'>
                        <i className='fas fa-heart'></i> 90
                    </div>
                </div>
                <div className='course-detail-info__teacher para'>
                    <i className='fas fa-chalkboard-teacher'></i> Nguyễn Văn C
                </div>
            </div>

            <div className='course-detail-main u-padding-body'>
                <input type='checkbox' id='toggle-list-lesson' />
                <label for='toggle-list-lesson' class='toggle-list-lesson'>
                    <span class='fa fa-bars'></span>
                </label>

                <div className='list-lesson'>
                    <div className='list-lesson__heading heading-primary'>
                        Danh sách bài
                    </div>
                    <div className='list-lesson__list'>
                        {lessons.map((lesson) => {
                            return (
                                <p
                                    className='lesson-item'
                                    onClick={() =>
                                        handleShowLesson(lesson.slug)
                                    }
                                    key={lesson.slug}>
                                    {lesson.name}
                                </p>
                            );
                        })}
                    </div>
                </div>
                <div className='lesson-detail'>
                    <ShowLesson course={course} />
                    <ShowComment avatar = {avatar} />
                </div>
            </div>
        </div>
    );
}

NMLT.propTypes = {};

export default NMLT;
