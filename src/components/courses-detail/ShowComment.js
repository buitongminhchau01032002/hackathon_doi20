import React from 'react';
import avatar from '../../img/avatar.png';
function ShowComment() {
    return (
        <div>
            <div className='ex'>
                <div className='ex__heading heading-primary'>Bài tập</div>
                <div className='ex__content'>
                    Các em làm bài này rồi gửi file c++ nhe. Đề bài: Viết chương
                    trình tính tổng 2 số
                </div>
                <a href='#' className='btn ex_btn'>
                    Nộp bài
                </a>
            </div>

            <div className='lesson-detail__comment'>
                <div className='type-comment'>
                    <div className='comment-avatar'>
                        <img src={avatar} alt='' className='comment-avatar-img' />
                    </div>
                    <div className='comment-form'>
                        <textarea
                            rows='3'
                            cols='50'
                            placeholder='Viết bình luận'
                            className='comment-form__textarea'></textarea>
                        <div>
                            <a href='' className='btn form-contact__submit'>
                                Bình luận
                            </a>
                        </div>
                    </div>
                </div>
                <div className='comment-content'>
                    <div className='comment-item'>
                        <div className='comment-item__acc'>
                            <div className='comment-avatar'>
                                <img
                                    src={avatar}
                                    alt=''
                                    className='comment-avatar-img'
                                />
                            </div>
                            <div className='comment-item__name-date'>
                                <div className='comment-name'>
                                    <div className='comment-name--name'>
                                        Nguyễn Văn A
                                    </div>
                                    <div className='comment-name--name--tag'></div>
                                </div>
                                <div className='comment-date'>2/2/2222</div>
                            </div>
                        </div>
                        <div className='comment-item__content para'>
                            Lorem ipsum dolor sit amet consectetur adipisicing
                            elit. Consequuntur hic odio voluptatem tenetur
                            consequatur. Vestibulum ante ipsum primis in
                            faucibus orci luctus etultrices posuere cubilia
                            Curae;
                        </div>
                    </div>

                    <div className='comment-item'>
                        <div className='comment-item__acc'>
                            <div className='comment-avatar'>
                                <img
                                    src={avatar}
                                    alt=''
                                    className='comment-avatar-img'
                                />
                            </div>
                            <div className='comment-item__name-date'>
                                <div className='comment-name'>
                                    <div className='comment-name--name'>
                                        Nguyễn Văn C
                                    </div>
                                    <div className='comment-name--name--tag'>
                                        Giảng viên
                                    </div>
                                </div>
                                <div className='comment-date'>2/2/2222</div>
                            </div>
                        </div>
                        <div className='comment-item__content para'>
                            Lorem ipsum dolor sit amet consectetur adipisicing
                            elit. Consequuntur hic odio voluptatem tenetur
                            consequatur. Vestibulum ante ipsum primis in
                            faucibus orci luctus etultrices posuere cubilia
                            Curae;
                        </div>
                    </div>

                    <div className='comment-item'>
                        <div className='comment-item__acc'>
                            <div className='comment-avatar'>
                                <img
                                    src={avatar}
                                    alt=''
                                    className='comment-avatar-img'
                                />
                            </div>
                            <div className='comment-item__name-date'>
                                <div className='comment-name'>
                                    <div className='comment-name--name'>
                                        Nguyễn Thị B
                                    </div>
                                    <div className='comment-name--name--tag'></div>
                                </div>
                                <div className='comment-date'>2/2/2222</div>
                            </div>
                        </div>
                        <div className='comment-item__content para'>
                            Lorem ipsum dolor sit amet consectetur adipisicing
                            elit. Consequuntur hic odio voluptatem tenetur
                            consequatur. Vestibulum ante ipsum primis in
                            faucibus orci luctus etultrices posuere cubilia
                            Curae;
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

ShowComment.propTypes = {};

export default ShowComment;
