import React from 'react'
import "../App.css"

function Row() {
    return (
        <div>
            <section className="section-main">
                <div className="section-main__content">
                    <h2 className="section-main__heading">
                        Tham gia môi trường học tập trực tuyến
                    </h2>
                            <p className="para section-main__text">
                                Kết nối với nền tảng kiến thức khổng lồ
                    </p>
                            <a href="#" className="btn section-main__btn">Tham gia ngay</a>
                        </div>
                    </section>
                    <section className="section-3card u-padding-body">
                        <div className="row row-3card">
                            <div className="col-sm-4 col-12">
                                <div className="card3-card">
                                    <div className="card3-card__icon"><i className="fas fa-chalkboard-teacher" /></div>
                                    <div className="card3-card__heading heading-primary">Dành cho giảng viên</div>
                                    <div className="card3-card__text para">
                                    Dễ dàng kết nối với học sinh và những giảng viên khác.
                        </div>
                                    <a href="#" className="card3-card__link">Tham gia ngay</a>
                                </div>
                            </div>
                            <div className="col-sm-4 col-12">
                                <div className="card3-card">
                                    <div className="card3-card__icon"><i className="fas fa-book-reader" /></div>
                                    <div className="card3-card__heading heading-primary">Dành cho học viên</div>
                                    <div className="card3-card__text para">
                                        Phương pháp học tập hiểu quả.
                        </div>
                                    <a href="#" className="card3-card__link">Tham gia ngay</a>
                                </div>
                            </div>
                            <div className="col-sm-4 col-12">
                                <div className="card3-card">
                                    <div className="card3-card__icon"><i className="fas fa-layer-group" /></div>
                                    <div className="card3-card__heading heading-primary">Giới thiệu nền tảng</div>
                                    <div className="card3-card__text para">
                                        Tiếp cận với những khóa học đầy đủ kiến thức. 
                        </div>
                                    <a href="#" className="card3-card__link">Tìm hiểu thêm</a>
                                </div>
                            </div>
                        </div>
                    </section>
        </div>
    );
}

export default Row
