import React from 'react';
import PropTypes from 'prop-types';

import { useState, useEffect } from 'react';
import CourseList from './CourseList';
import Pagination from './Pagination';
import SearchForm from './SearchForm';
import nonAccentVietnamese from '../nonAccentVietnamese';
import courses from '../db';
function Courses(props) {
    const limit = 6;
    const initialCourses = [];
    for (let index = 0; index < 6; index++) {
        initialCourses.push(courses[index]);
    }
    const [courseList, setCourseList] = useState(initialCourses);
    const [pageNumber, setPageNumber] = useState(1);
    const [pageTotal, setPageTotal] = useState(
        Math.ceil(courses.length / limit),
    );
    const [searchTerm, setSearchTerm] = useState('');
    const [searchResults, setSearchResults] = useState([]);
    const [searchCourses, setSearchCourses] = useState([]);
    const handleChange = (event) => {
        setSearchTerm(event.target.value);
    };
    const handlePage = (pageNumber) => {
        if (searchTerm) {
            handleSearchPageChange(pageNumber);
        } else handlePageChange(pageNumber);
    };
    const handlePageChange = (pageNumber) => {
        let newCourses = [];
        for (
            let index = (pageNumber - 1) * limit;
            index <
            (pageNumber === pageTotal ? courses.length : pageNumber * limit);
            index++
        ) {
            newCourses.push(courses[index]);
        }
        setCourseList(newCourses);
        setPageNumber(pageNumber);
    };
    const handleSearchPageChange = (pageNumber) => {
        let newCourses = [];
        for (
            let index = (pageNumber - 1) * limit;
            index <
            (pageNumber === pageTotal
                ? searchCourses.length
                : pageNumber * limit);
            index++
        ) {
            newCourses.push(courses[index]);
        }
        setSearchResults(newCourses);
        setPageNumber(pageNumber);
    };
    useEffect(() => {
        const searchCourses = courses.filter((course) =>
            nonAccentVietnamese(course.name).toLowerCase().includes(searchTerm),
        );
        if (searchTerm) {
            setPageNumber(1);
            setPageTotal(Math.ceil(searchCourses.length / limit));
            setSearchCourses(searchCourses);
            const results = [];
            for (
                let index = 0;
                index <
                (limit > searchCourses.length ? searchCourses.length : limit);
                index++
            ) {
                results.push(searchCourses[index]);
            }
            setSearchResults(results);
        } else {
            setPageTotal(Math.ceil(courses.length / limit));
            handlePage(1);
        }
    }, [searchTerm]);

    return (
        <div>
            <SearchForm handleChange={handleChange} searchTerm={searchTerm} />
            <CourseList courses={searchTerm ? searchResults : courseList} />
            <Pagination
                pageNumber={pageNumber}
                handlePageChange={handlePage}
                pageTotal={pageTotal}
            />
        </div>
    );
}

Courses.propTypes = {};

export default Courses;
