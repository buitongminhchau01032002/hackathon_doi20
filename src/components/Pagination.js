import React from 'react';
import PropTypes from 'prop-types';

function Pagination({ pageNumber, handlePageChange, pageTotal }) {
    const subtractPage = () => {
        handlePageChange(pageNumber - 1);
    };
    const addPage = () => {
        handlePageChange(pageNumber + 1);
    };
    return (
        <div className='pagination'>
            <button
                className='pagination__btn'
                onClick={subtractPage}
                disabled={pageNumber === 1 ? true : false}>
                <i className='fas fa-chevron-left'></i>
            </button>
            <p className='pagination__num'>{pageNumber}/{pageTotal}</p>
            <button
                className='pagination__btn'
                onClick={addPage}
                disabled={pageNumber === pageTotal  ? true : false}>
                <i className='fas fa-chevron-right'></i>
            </button>
        </div>
    );
}

Pagination.propTypes = {};

export default Pagination;
