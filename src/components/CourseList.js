import React from 'react';
import PropTypes from 'prop-types';

import Course from './Course';
function CourseList({ courses }) {
    return (
        <div>
            <section className='section-courses u-padding-body'>
                <h2 className='section-courses__heading heading-primary'>
                    Khoá học của tôi
                </h2>
                <div className='row row-card-course'>
                    {courses.map((course) => (
                        <Course key={course.id} course={course} />
                    ))}
                </div>
            </section>
        </div>
    );
}

CourseList.propTypes = {};

export default CourseList;
